import { Injectable } from '@angular/core';

import { Livre } from '../models/livre';
import { CD } from '../models/cd';

import { mock_livres } from '../mocks/livres';
import { mock_cds } from '../mocks/cds';
import { DocumentBibliotheque } from '../models/document-bibliotheque';

@Injectable()
export class DocumentsService {

  public livres: Array<Livre>;
  public cds : Array<CD>;

  constructor() {
    // Initialisation des données
    this.livres = mock_livres;
    this.cds = mock_cds;
  }

  preter(document: DocumentBibliotheque) {
    (document.estDisponible)? document.emprunter() : document.rendre();
  }

}
