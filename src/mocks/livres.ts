import { Livre } from "../models/livre";

export const mock_livres: Livre[] = [
    new Livre("Le Petit Prince", "Antoine de Saint-Exupéry", true),
    new Livre("Les Aventures d'Alice au pays des merveilles", "Lewis Carroll", true),
    new Livre("Harry Potter à l'école des sorciers", "J. K. Rowling", false),
    new Livre("Harry Potter et la Chambre des secrets", "J. K. Rowling", true),
    new Livre("Harry Potter et le Prisonnier d'Azkaban", "J. K. Rowling", true),
    new Livre("Harry Potter et la Coupe de feu", "J. K. Rowling", true),
    new Livre("Harry Potter et l'Ordre du phénix", "J. K. Rowling", true),
    new Livre("Harry Potter et le Prince de sang-mêlé", "J. K. Rowling", true),
    new Livre("Harry Potter et les Reliques de la Mort", "J. K. Rowling", false)
]