import { CD } from "../models/cd";

export const mock_cds: CD[] = [
    new CD("J'en parlerai au diable", "Johnny Hallyday", true),
    new CD("Shallow", "Lady Gaga", true),
    new CD("Pardonne-moi", "Johnny Hallyday", true),
    new CD("A nos souvenirs", "Trois Cafés Gourmands", true),
    new CD("Promises", "Calvin Harris", false),
    new CD("Ramenez la coupe à la maison", "Vegedream", true),
    new CD("I'll Never Love Again", "Lady Gaga", false),
    new CD("Jaloux", "Dadju", true)
]