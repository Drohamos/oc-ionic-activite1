import { DocumentBibliotheque } from "./document-bibliotheque";

export class CD extends DocumentBibliotheque {
    artiste: string;

    constructor(titre, artiste, estDisponible) {
        super(titre, estDisponible);

        this.artiste = artiste;
    }
}