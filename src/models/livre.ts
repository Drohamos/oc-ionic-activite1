import { DocumentBibliotheque } from "./document-bibliotheque";

export class Livre extends DocumentBibliotheque {
    auteur: string;

    constructor(titre, auteur, estDisponible) {
        super(titre, estDisponible);

        this.auteur = auteur;
    }
}