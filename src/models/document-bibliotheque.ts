export class DocumentBibliotheque {
    titre: string;
    estDisponible: boolean;

    constructor(titre, estDisponible) {
        this.titre = titre;
        this.estDisponible = estDisponible;
    }

    public emprunter() {
        this.estDisponible = false
    }

    public rendre() {
        this.estDisponible = true;
    }
}