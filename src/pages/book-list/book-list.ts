import { Component } from '@angular/core';
import { IonicPage, ModalController } from 'ionic-angular';

import { Livre } from '../../models/livre';
import { DocumentsService } from '../../services/documents.service';

@IonicPage()
@Component({
  selector: 'page-book-list',
  templateUrl: 'book-list.html',
})
export class BookListPage {

  livres: Array<Livre>;

  constructor(private documentsSrv: DocumentsService, private modalCtrl: ModalController) {
    this.livres = this.documentsSrv.livres;
  }
  
  bookSelected(livre: Livre) {
    let modal = this.modalCtrl.create('LendBookPage', { livre: livre });
    modal.present();
  }

}
