import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';
import { Livre } from '../../models/livre';

@IonicPage()
@Component({
  selector: 'page-lend-book',
  templateUrl: 'lend-book.html',
})
export class LendBookPage {

  livre: Livre;

  constructor(public navParams: NavParams, private viewCtrl: ViewController) {
    this.livre = this.navParams.data.livre;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
