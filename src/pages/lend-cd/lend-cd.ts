import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';
import { CD } from '../../models/cd';

@IonicPage()
@Component({
  selector: 'page-lend-cd',
  templateUrl: 'lend-cd.html',
})
export class LendCdPage {

  cd: CD;

  constructor(public navParams: NavParams, private viewCtrl: ViewController) {
    this.cd = this.navParams.data.cd;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
